import React from 'react';
import ContentData from './ContentData'

const ProfileData = (props) => {
    return (
        <div>
            <p>
                <a className="btn btn-primary" data-toggle="collapse" href="#collapseExample">
                   Informations sur {props.welcome.name} ?
                </a>
            </p>

            <ContentData  />
        </div>
    )
}

export default ProfileData;